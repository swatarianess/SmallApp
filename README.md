# SmallApp - SpringBoot CSV Application

SmallApp is a small application that allows uploading and getting csv file data.

## Getting Started
These instructions are for getting the project up and running locally.

### Prerequisites

- JDK 1.8 or later (Built with JDK1.17)
- Gradle 4+

### Running the Application Locally

1. Navigate to the project directory:

```sh
cd SmallApp
```

2. Build the project using Gradle:

```sh
./gradlew build
```

3. Run the application using Gradle:

```sh
./gradlew bootRun
```

The application should now be running on <http://localhost:8080>.

## Usage

### Uploading a CSV File

To upload a CSV file, send a `multipart/form-data` POST request to `/records/upload` with the CSV file included in the form data:

```sh
curl -F 'file=@/path/to/your/file.csv' http://localhost:8080/records/upload
```

### Fetching Records by Code

To fetch records with a specific code, use the GET endpoint `/records` with the `code` query parameter:

```http
GET /records?code=1
```

This will return the record with the code `1`.

### Deleting All Records

To delete all records, send a DELETE request to `/records/delete-all`:

```sh
curl -X DELETE http://localhost:8080/api/records/delete-all
```