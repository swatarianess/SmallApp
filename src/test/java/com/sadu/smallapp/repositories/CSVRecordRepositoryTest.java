package com.sadu.smallapp.repositories;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.sadu.smallapp.models.*;
import java.util.*;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.autoconfigure.orm.jpa.*;
import org.springframework.dao.*;

@DataJpaTest
class CSVRecordRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private CSVRecordRepository csvRecordRepository;

    @Test
    public void testFindByCode_Found() {
        // Setup data
        CSVRecord record = new CSVRecord();
        record.setCode("12345");
        record.setDisplayValue("Test Value");
        entityManager.persist(record);
        entityManager.flush();

        // Test find by code
        CSVRecord found = csvRecordRepository.findByCode(record.getCode()).stream().findFirst().orElse(null);

        // Validate
        assertThat(found).isNotNull();
        assertThat(found.getCode()).isEqualTo(record.getCode());
    }

    @Test
    public void testFindByCode_NotFound() {
        String code = "nonexistent";

        List<CSVRecord> found = csvRecordRepository.findByCode(code);

        assertThat(!found.isEmpty()).isFalse();
    }

    @Test
    public void testSave_DuplicateCode() {
        CSVRecord record1 = new CSVRecord();
        record1.setCode("uniqueCode");
        record1.setDisplayValue("First Record");
        csvRecordRepository.save(record1);

        CSVRecord record2 = new CSVRecord();
        record2.setCode("uniqueCode");
        record2.setDisplayValue("Second Record");

        assertThrows(DataIntegrityViolationException.class, () -> {
            csvRecordRepository.saveAndFlush(record2);
        });
    }


}