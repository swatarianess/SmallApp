package com.sadu.smallapp.controllers;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.StringContains.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.sadu.smallapp.models.*;
import com.sadu.smallapp.services.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

@ExtendWith(SpringExtension.class)
@WebMvcTest(FileController.class)
class FileControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CSVService csvService;


    @Test
    void testUploadFile() throws Exception{
        MockMultipartFile file = new MockMultipartFile(
            "file",
            "test.csv",
            MediaType.TEXT_PLAIN_VALUE,
            "source,codeListCode,code,displayValue,longDescription,fromDate,toDate,sortingPriority\nZIB,ZIB001,271636001,Regular,Description,01-01-2019,,1".getBytes()
        );

        mockMvc.perform(multipart("/records/upload")
            .file(file))
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("File uploaded successfully and data stored.")));

    }

    @Test
    public void testGetRecordsByCode() throws Exception {
        CSVRecord mockRecord = new CSVRecord();
        mockRecord.setCode("271636001");

        List<CSVRecord> recordList = List.of(mockRecord);

        when(csvService.findRecordsByCode(any())).thenReturn(recordList);

        mockMvc.perform(get("/records")
                .param("code", "271636001"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].code", equalTo("271636001")));
    }


    @Test
    public void testDeleteAllRecords() throws Exception {
        doNothing().when(csvService).deleteAllRecords();

        mockMvc.perform(delete("/records/delete-all"))
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("All records have been deleted successfully.")));
    }
}