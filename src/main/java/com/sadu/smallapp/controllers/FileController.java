package com.sadu.smallapp.controllers;

import com.sadu.smallapp.models.CSVRecord;
import com.sadu.smallapp.services.*;
import jakarta.servlet.http.*;
import java.util.*;
import org.apache.commons.csv.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.dao.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.*;

@RestController
@RequestMapping("/records")
public class FileController {

    @Autowired
    private CSVService csvService;

    private static final Logger logger = LoggerFactory.getLogger(FileController.class);

    @PostMapping("/upload")
    public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile file) {

        if (file.isEmpty()){
            return ResponseEntity.badRequest().body("No CSV file was provided or the file was empty.");
        }

        try {
            csvService.parseCSVFileAndStore(file);
            return ResponseEntity.ok().body("File uploaded successfully and data stored.");
        } catch (DataIntegrityViolationException e) {
            logger.error("Data integrity violation - duplicate code found: " + e.getMessage());
            // Return a conflict status to indicate that data in the request was valid but would cause a duplicate entry
            return ResponseEntity.status(409).body("Upload failed: Duplicate 'code' values found.");
        } catch (Exception e) {
            logger.error("Error processing file upload: " + e.getMessage());
            return ResponseEntity.internalServerError().body("Upload failed: An error occurred processing the file.");
        }
    }

    @PostMapping()
    public ResponseEntity<?> addRecord(@RequestBody CSVRecord record){
        try {
            CSVRecord savedRecord = csvService.saveRecord(record);
            return ResponseEntity.ok(savedRecord);
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error saving record: " + e.getMessage());
        }
    }

    @GetMapping()
    public ResponseEntity<List<CSVRecord>> getRecordsByCode(@RequestParam(value = "code") String code) {
        List<CSVRecord> records = csvService.findRecordsByCode(code);
        if (records.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(records);
    }

    @GetMapping("/csv")
    public void getAllRecordsInCSV(HttpServletResponse response) {
        List<CSVRecord> records = csvService.getAllRecords();
        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"records.csv\"");

        try (CSVPrinter csvPrinter = new CSVPrinter(response.getWriter(), CSVFormat.DEFAULT)) {
            for (CSVRecord record : records) {
                csvPrinter.printRecord(record.getSource(), record.getCodeListCode(), record.getCode(),
                    record.getDisplayValue(), record.getLongDescription(), record.getFromDate(),
                    record.getToDate(), record.getSortingPriority());
            }
        } catch (Exception e) {
            throw new RuntimeException("Failed to export data to CSV", e);
        }
    }

    @DeleteMapping("/delete-all")
    public ResponseEntity<String> deleteAllRecords() {
        try {
            csvService.deleteAllRecords();
            return ResponseEntity.ok("All records have been deleted successfully.");
        } catch (Exception e) {
            return ResponseEntity.internalServerError().body("Failed to delete records: " + e.getMessage());
        }
    }

}
