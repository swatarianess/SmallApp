package com.sadu.smallapp.services;

import com.sadu.smallapp.models.CSVRecord;
import com.sadu.smallapp.repositories.*;
import java.io.*;
import java.nio.charset.*;
import java.text.*;
import java.util.*;
import org.apache.commons.csv.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.dao.*;
import org.springframework.stereotype.*;
import org.springframework.web.multipart.*;

@Service
public class CSVService {

    @Autowired
    private CSVRecordRepository repository;

    private static final Logger logger = LoggerFactory.getLogger(CSVService.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public List<CSVRecord> findRecordsByCode(String code){
        return repository.findByCode(code);
    }

    public CSVRecord saveRecord(CSVRecord record){
        return repository.save(record);
    }

    public void deleteAllRecords() {
        repository.deleteAll();
    }

    public List<CSVRecord> getAllRecords() {
        return repository.findAll();
    }

    public void parseCSVFileAndStore(final MultipartFile file) {
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(file.getInputStream(), StandardCharsets.UTF_8));
            CSVParser csvParser = new CSVParser(fileReader, CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim())) {

            List<org.apache.commons.csv.CSVRecord> csvRecords = csvParser.getRecords();
            List<CSVRecord> csvRecordEntities = new ArrayList<>();

            for (org.apache.commons.csv.CSVRecord record : csvRecords) {
                CSVRecord csvRecordEntity = new CSVRecord();
                csvRecordEntity.setSource(record.get("source"));
                csvRecordEntity.setCodeListCode(record.get("codeListCode"));
                csvRecordEntity.setCode(record.get("code"));
                csvRecordEntity.setDisplayValue(record.get("displayValue"));
                csvRecordEntity.setLongDescription(record.get("longDescription"));

                try {
                    csvRecordEntity.setFromDate(dateFormat.parse(record.get("fromDate")));
                } catch (Exception e){
                    csvRecordEntity.setFromDate(null);
                }

                try {
                    if (!record.get("toDate").isEmpty()){
                        csvRecordEntity.setToDate(record.isSet("toDate") && !record.get("toDate").isEmpty() ? dateFormat.parse(record.get("toDate")) : null);
                    }
                } catch (Exception e){
                    csvRecordEntity.setToDate(null);
                }


                try{
                    csvRecordEntity.setSortingPriority(Integer.parseInt(record.get("sortingPriority")));
                } catch (NumberFormatException e ){
                    csvRecordEntity.setSortingPriority(null);
                }

                csvRecordEntities.add(csvRecordEntity);
            }

            repository.saveAll(csvRecordEntities);
            logger.info("Successfully uploaded data to database.");
        } catch (DataIntegrityViolationException e) {
            logger.error("Failed to save some records due to duplicate codes.", e);
        } catch (Exception e){
            logger.error("Error processing file: " + e.getMessage(), e);
        }
    }
}
