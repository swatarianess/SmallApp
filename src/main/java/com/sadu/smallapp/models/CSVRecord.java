package com.sadu.smallapp.models;

import jakarta.persistence.*;
import java.util.*;

@Entity
@Table(name = "csvrecord")
public class CSVRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String code;

    private String source;
    private String codeListCode;
    private String displayValue;
    private String longDescription;
    private Date fromDate;
    private Date toDate;
    private Integer sortingPriority;

    // Getters
    public Long getId() { return id; }
    public String getSource() { return source; }
    public String getCodeListCode() { return codeListCode; }
    public String getCode() { return code; }
    public String getDisplayValue() { return displayValue; }
    public String getLongDescription() { return longDescription; }
    public Date getFromDate() { return fromDate; }
    public Date getToDate() { return toDate; }
    public Integer getSortingPriority() { return sortingPriority; }

    // Setters
    public void setSource(String source) { this.source = source; }
    public void setCodeListCode(String codeListCode) { this.codeListCode = codeListCode; }
    public void setCode(String code) { this.code = code; }
    public void setDisplayValue(String displayValue) { this.displayValue = displayValue; }
    public void setLongDescription(String longDescription) { this.longDescription = longDescription; }
    public void setFromDate(Date fromDate) { this.fromDate = fromDate; }
    public void setToDate(Date toDate) { this.toDate = toDate; }
    public void setSortingPriority(Integer sortingPriority) { this.sortingPriority = sortingPriority; }
}
