package com.sadu.smallapp.repositories;

import com.sadu.smallapp.models.*;
import java.util.*;
import org.springframework.data.jpa.repository.*;

public interface CSVRecordRepository extends JpaRepository<CSVRecord, Long> {
    List<CSVRecord> findByCode(String code);
}
